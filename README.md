# Zsh on OSX

## Установка Homebrew
`/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"` 

## Установка Iterm2
![iterm2](https://www.iterm2.com/img/logo2x.jpg)
`brew cask install iterm2`

## Установка Zsh
`brew install zsh`

Файл конфига в `~/.zshrc`

Изменить дефолтный шелл на zsh `chsh -s /bin/zsh`
# Oh My Zsh
![oh-my-zsh](https://camo.githubusercontent.com/5c385f15f3eaedb72cfcfbbaf75355b700ac0757/68747470733a2f2f73332e616d617a6f6e6177732e636f6d2f6f686d797a73682f6f682d6d792d7a73682d6c6f676f2e706e67)

Отличный фреймворк для zsh с кучей полезных расширений

Полезные плагины : 

[zsh-nvm](https://github.com/lukechilds/zsh-nvm)

`git clone https://github.com/lukechilds/zsh-nvm ~/.oh-my-zsh/custom/plugins/zsh-nvm`

[Zsh Syntax Highlighting](https://github.com/zsh-users/zsh-syntax-highlighting)

` git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting`

[Zsh Autosuggestions](https://github.com/zsh-users/zsh-autosuggestions)

`git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions`

# Nerd fonts
Для иконок в терминале нужны [Nerd Fonts](https://github.com/ryanoasis/nerd-fonts#font-installation)

# iTerm2 настройки шрифтов
`iTerm2 -> Preferences -> Profiles -> Text -> Font -> Change Font`

нужна галоачка `Use a different font for non-ASCII text`
![iterm-font-settings](https://i.imgur.com/4GXG60i.png)

# Powerlevel9k
![powerlvl9k](https://raw.githubusercontent.com/bhilburn/powerlevel9k-logo/master/logo-banner.png)

`git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/custom/themes/powerlevel9k`

# Colorls
![colorls](https://i.imgur.com/Awj84sg.png)
### [Установка Ruby](https://www.ruby-lang.org/en/documentation/installation/)
### [Color LS](https://github.com/athityakumar/colorls)
